package com.sourceit.l9test;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {
    private static final String ENDPOINT = "http://resources.finance.ua/ru/public";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/currency-cash.json")
        void getOrganization(Callback<CurrencyResponse> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getOrganization(Callback<CurrencyResponse> callback) {
        apiInterface.getOrganization(callback);
    }
}
