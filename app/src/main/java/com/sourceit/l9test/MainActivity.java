package com.sourceit.l9test;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sourceit.l9test.CurrencyResponse.Organization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    public static final String FILE_NAME = "my_file";
    List<Organization> list = new ArrayList<>();
    RecyclerView rList;
    OrganizationAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        rList = (RecyclerView) findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rList.setLayoutManager(layoutManager);
        adapter = new OrganizationAdapter(this, list);
        rList.setAdapter(adapter);

        if (fileExistance(FILE_NAME)) {
            readData();
        } else {
            getData();
        }
    }

    private void getData() {
        Retrofit.getOrganization(new Callback<CurrencyResponse>() {
            @Override
            public void success(CurrencyResponse currencyResponse, Response response) {
                //about 1sec
                updateUI(currencyResponse.organizations);
                saveData(currencyResponse.organizations);
            }


            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "error",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveData(List<Organization> organizations) {
        FileOutputStream fos = null; //open stream for writing data to file_name file
        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String json = new Gson().toJson(organizations); //generate json
            fos.write(json.getBytes()); //write bites array
            fos.close(); //important: close stream
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }


    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);//open stream for reading data from file_name file
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis)); //create reader from stream
            String line;
            while ((line = reader.readLine()) != null) { // get text line by line, while text presen
                json.append(line); // save text line to StringBuilder
            }
            fis.close(); //important: close reader
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<Organization>>(){}.getType();
        List<Organization> organizations = new Gson().fromJson(json.toString(), listType);
        updateUI(organizations);
    }


    private void updateUI(List<Organization> organizations) {
        list.addAll(organizations);
        adapter.notifyDataSetChanged();
    }

    private boolean fileExistance(String fname){
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }


}