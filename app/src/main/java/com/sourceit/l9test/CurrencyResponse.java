package com.sourceit.l9test;


import android.provider.ContactsContract;

import java.util.List;

public class CurrencyResponse {

    String date;
    List<Organization> organizations;

    public static class Organization{
        public String title;
        public String address;
        public String phone;
    }


}
