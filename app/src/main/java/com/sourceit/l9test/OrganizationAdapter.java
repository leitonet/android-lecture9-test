package com.sourceit.l9test;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.List;

import static com.sourceit.l9test.CurrencyResponse.*;


public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.ViewHolder> {

    private List<CurrencyResponse.Organization> organizations;
    private Context context;

    public OrganizationAdapter(Context context, List<Organization> organizations) {
        this.organizations = organizations;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_organization, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {
        Organization org = organizations.get(pos);
        viewHolder.title.setText(org.title);
        viewHolder.address.setText(org.address);
        viewHolder.tel.setText(org.phone);
    }

    @Override
    public int getItemCount() {
        return organizations.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView address;
        private TextView tel;

        public ViewHolder(View item) {
            super(item);
            title = (TextView) item.findViewById(R.id.title);
            address = (TextView) item.findViewById(R.id.address);
            tel = (TextView) item.findViewById(R.id.tel);
        }
    }
}
